import Vue from 'vue'
import Router from 'vue-router'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import Material from '@/components/Material'
import MaterialForm from '@/components/MaterialForm'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Material',
      component: Material
    },
    {
      path: '/create',
      name: 'MaterialForm',
      component: MaterialForm
    },
    {
      path: '/:id',
      name: 'MaterialEdit',
      component: MaterialForm
    }

  ]
})
