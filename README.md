# TugasAkhir

Tugas akhir ini adalah sebuah aplikasi material yang sangat sederhana guna untuk melengkapi tugas pemrograman web saya
Disini kita bisa Create, Read, Update dan Delete data barang yang tersedia di sebuah material

localhostnya menyesuaikan, disini saya portnya 8080

langkah langkah

##Membuat Setup Project Vue 

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

# Panduan Menggunakan Aplikasi

## 1. Beranda
Beranda ini berisi data data material yang ada
![Beranda](pict/pwd1.PNG)

## 2. Create Data
Ini adalah halaman untuk anda menambahkan data
![CreateData](pict/pwd2.PNG)

Terima Kasih :)
