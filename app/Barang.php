<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model {
    public $table = 'barang';

    protected $fillable = [
        'nama_barang', 'harga', 'stok', 'satuan'
    ];
}

?>